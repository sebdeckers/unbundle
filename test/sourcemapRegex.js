const assert = require('assert')

// Not exported, just copied. The regex is not directly part of the public
// interface. It is easier to write a few unit tests than lots of fixtures.
const sourcemapRegex = /\/\/[#@] sourceMappingURL=(?!\w+:)([^\s]+)$/

const fixtures = [
  {
    given: 'foo\n//# sourceMappingURL=foo.js.map',
    expected: 'foo.js.map'
  },
  {
    given: 'foo\n//# sourceMappingURL=foo.js.map ',
    expected: false
  },
  {
    given: 'foo\n//# sourceMappingURL=foo%20bar.js.map',
    expected: 'foo bar.js.map'
  },
  {
    given: 'foo\n//# sourceMappingURL=foo.js.map\nbar',
    expected: false
  },
  {
    given: 'foo\n//# sourceMappingURL=../bar/foo.js.map',
    expected: '../bar/foo.js.map'
  },
  {
    given: 'foo\n//# sourceMappingURL=data:foo.js.map',
    expected: false
  },
  {
    given: 'foo\n//# sourceMappingURL=http://foo.js.map',
    expected: false
  },
  {
    given: 'foo\n//# sourceMappingURL=https://foo.js.map',
    expected: false
  }
]

for (const { given, expected } of fixtures) {
  const actual = sourcemapRegex.test(given) && decodeURI(RegExp.$1)
  assert.strictEqual(actual, expected)
}
