const { test } = require('blue-tape')
const { fork } = require('child_process')
require('events.once/polyfill')
const { once } = require('events')
const { join } = require('path')
const { dir } = require('tmp')
const { promises: { readFile } } = require('fs')
const recursiveReaddir = require('recursive-readdir')

let destination, cleanup
test('Prepare temp directory', (t) => {
  dir({ unsafeCleanup: true }, (error, path, callback) => {
    destination = path
    cleanup = callback
    t.end(error)
  })
})

test('Run the CLI', async (t) => {
  const pkg = require('../package.json')
  const script = join(__dirname, '..', pkg.bin.unbundle)
  const entry = join(__dirname, 'fixtures/dummy-app/given/src/entry.js')
  const args = [
    '--entry', entry,
    '--destination', destination,
    '--verbose'
  ]
  const child = fork(script, args, {})
  const [code] = await once(child, 'exit')
  t.is(code, 0)
})

test('Compare output', async (t) => {
  const fixtures = join(__dirname, 'fixtures/dummy-app/expected')
  const expected = (await recursiveReaddir(fixtures)).sort()
  const actual = (await recursiveReaddir(destination)).sort()
  t.deepEqual(
    actual.map((path) => path.substr(destination.length)),
    expected.map((path) => path.substr(fixtures.length))
  )
  for (let i = 0; i < actual.length; i++) {
    const [actualData, expectedData] = await Promise.all([
      readFile(actual[i]),
      readFile(expected[i])
    ])
    t.deepEqual(actualData, expectedData)
  }
})

test('Clean up temp direcory', (t) => cleanup(t.end))
