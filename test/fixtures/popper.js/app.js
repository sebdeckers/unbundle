/* eslint-env browser */
import Popper from 'popper.js'

var reference = document.querySelector('button')
var popper = document.querySelector('#popper')
var anotherPopper = new Popper(
  reference,
  popper,
  {
    // popper options here
  }
)

console.log(anotherPopper)
