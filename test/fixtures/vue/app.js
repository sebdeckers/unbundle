/* eslint-env browser */
import Vue from 'vue/dist/vue.esm.browser.js'

var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
})

console.log(app)
