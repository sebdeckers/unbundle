/* eslint-env browser */
import { loadStripe } from '@stripe/stripe-js'

document.addEventListener('DOMContentLoaded', async () => {
  const publishableKey = 'pk_test_TYooMQauvdEDq54NiTphI7jx'
  const stripe = await loadStripe(publishableKey)
  var elements = stripe.elements()
  var cardElement = elements.create('card')
  cardElement.mount('#card-element')
  cardElement.on('ready', (event) => {
    document.getElementById('status').textContent = 'READY'
  })
})
