/* eslint-env browser */
import * as R from 'ramda'

const print = x => {
  document.body.append(
    document.createTextNode(x)
  )
}

R.forEach(print, [1, 2, 3])
