/* eslint-env browser */
import { fetch as whatwgFetch } from 'whatwg-fetch'

whatwgFetch('package.json')
  .then((response) => response.text())
  .then((text) => {
    document.body.textContent = text
  })
