const { test } = require('blue-tape')
const { join } = require('path')
const { readFileSync } = require('fs')
const unbundle = require('../unbundle.js')
const sortBy = require('lodash.sortby')

const scenarios = [
  {
    label: 'Contrived example using a dummy app',
    given: 'fixtures/dummy-app/given',
    expected: 'fixtures/dummy-app/expected',
    entry: 'src/entry.js',
    fixtures: [
      {
        from: 'node_modules/sister-module/sister-module.js',
        to: 'node_modules/sister-module/sister-module.js'
      },
      {
        from: 'node_modules/npm-module/npm-module.js',
        to: 'node_modules/npm-module/npm-module.js'
      },
      {
        from: 'src/child-module/bar.js',
        to: 'child-module/bar.js'
      },
      {
        from: 'src/child-module/foo.js',
        to: 'child-module/foo.js'
      },
      {
        from: 'src/entry.js',
        to: 'entry.js',
        map: 'entry.js.map'
      }
    ]
  },
  {
    label: 'Dynamic imports with string literals',
    given: 'fixtures/dynamic-import/given',
    expected: 'fixtures/dynamic-import/expected',
    entry: 'entry.js',
    fixtures: [
      {
        from: 'entry.js',
        to: 'entry.js'
      },
      {
        from: 'foo.js',
        to: 'foo.js'
      }
    ]
  }
]

for (const scenario of scenarios) {
  test(scenario.label, async (t) => {
    const entry = join(__dirname, scenario.given, scenario.entry)
    const actual = unbundle(entry, { verbose: true })
    const expected = scenario.fixtures.map(({ from, to, map = false }) => {
      const codePath = join(__dirname, scenario.expected, to)
      const code = readFileSync(codePath).toString()
      return { code, map, to, from: join(__dirname, scenario.given, from) }
    })
    t.deepEqual(
      sortBy(Array.from(actual.values()), 'from'),
      sortBy(expected, 'from')
    )
  })
}
