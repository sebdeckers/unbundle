# Unbundle 💠

Unbundle traces JavaScript dependencies in your code to resolve `export`/`import`/`import()` paths. The output files contain relative file paths, as supported by modern browsers.

Unbundle follows the UNIX philosophy of doing one job well. Use other tools to further process the files for minification, revving, auditing, server pushing, etc.

## Demo

The Commons Host dashboard web app uses Unbundle. See the `build` script in its `package.json` file.

Code:
https://gitlab.com/commonshost/website

## Example

```
$ unbundle --entry app.js --destination dist
```

Where `app.js` is a source code entry file and `dist` is a to-be-created output directory.

Input: `app.js` uses unresolved paths and named imports

```js
import widget from './widget'
import moment from 'moment'
const foobar = import('./foobar')
// ...
```

Output: `dist/app.js` has its import paths resolved

```js
import widget from './widget.js'
import lodash from '/node_modules/moment/src/moment.js'
const foobar = import('./foobar.mjs')
// ...
```

All sub-dependencies are recursively traced and written to the output directory in the correct location. Named dependencies, i.e. packages from NPM, are moved to a '/node_modules` sub-directory. Any NPM package that publishes ECMAScript modules (ESM) should work. See below for compatibility testing.

The resulting output directory looks like:

- dist/
  - app.js
  - widget.js
  - foobar.mjs
  - node_modules/moment/src/
    - moment.js
    - lib/moment/moment.js
    - lib/moment/calendar.js
    - lib/locale/locale.js
    - lib/duration/duration.js
    - lib/units/units.js
    - lib/utils/is-date.js
    - ...

Any external source maps are also copied to the destination directory. They are only downloaded when the browser development tools are opened, so no performance penalty.

## Why?

To make NPM packages (i.e. `node_modules`) work on the web with the least amount of effort.

Tools like [Browserify](http://browserify.org), [Webpack](https://webpack.js.org), and [Rollup](https://rollupjs.org) exist to transpile various languages and module formats into a single, concatenated bundle. Depending on the use-case, configuring these tools and optimising their output requires significant effort and expertise.

Unbundle is a tiny tool. About 50 significant lines of code (SLoC). Standalone it is useful; composed with others it is powerful. That is the Unix philosophy of minimalist, modular, reusable tools.

Unbundle is designed for, but not limited to, delivering web apps with protocols like HTTP/2 Server Push. Other tools can be used to provide file revving for cache busting, code minification, AMD/UMD/CommonJS to ES2015 module conversion, Flow/TypeScript language transpilation, and more.

Earlier, defunct versions of Unbundle, anno 2016, did too much at once: support for JSX and Flow, injecting service workers with Cache Digest and `Cache-Control: immutable`, file watching, multi-CPU cluster support, CLI progress reporting, file revving for cache busting, code minification, source maps, and more. That caused it to be good at exactly one workflow and wrong, to varying degrees, for almost everything else.

## API

```
const files = unbundle(entry, options)
```

`entry` is the path of a source file to transform, and optionally trace and transform its dependencies.

`options` is an object containing the properties:

- `recurse` - Process all discovered dependencies. Default: `true`
- `root` - Prefix a custom path to the NPM dependencies in `node_modules`. Default: `/`
- `verbose` - Print information to the console. Default: `false`

`files` is a Map containing results. Keys are the `source` path. Values have the properties:

- `code` - Transformed source code.
- `from` - Absolute input file path.
- `to`- Relative output file path.
- `map` - File name of the source map.

## CLI

```
unbundle [options]
```

### Options

#### `-i`, `--entry` `<file>`

- Required: Yes
- Type: String

File path of source code entry point.

#### `-o`, `--destination` `<directory>`

- Required: Yes
- Type: String

Directory path to write output.

#### `--root`

- Default: `"/"`

Prefix path for `node_modules`

#### `-f`, `--force`

- Default: `false`

Overwrite existing files.

#### `-r`, `--recurse`

- Default: `true`

Recursively trace all files.

#### `--verbose`

- Default: `false`

Show more information during file processing.

#### `--version`

Show version number

#### `--help`

Show version number

### Examples

Trace all dependencies of `src/app.js` and output to `dist` directory.

```
unbundle --entry ./src/app.js --destination ./dist
```

Prefix imports of NPM packages with: `/assets/scripts/node_modules/`

```
unbundle --entry index.js --destination public/assets/scripts --root /assets/scripts/
```

## Compatibility

| Package | Status | Notes |
|-|-|-|
| [angular](https://github.com/angular/angular) | ❔ | Maybe? |
| [choreographer-router](https://www.npmjs.com/package/choreographer-router) | ✅ |
| [d3](https://www.npmjs.com/package/d3) | ✅ |
| [graphql](https://www.npmjs.com/package/graphql) | ❌ | [graphql/graphql-js#1819](https://github.com/graphql/graphql-js/issues/1819) |
| [lit-element](https://www.npmjs.com/package/lit-element) | ✅ |
| [lit-html](https://www.npmjs.com/package/lit-html) | ✅ |
| [lodash-es](https://www.npmjs.com/package/lodash-es) | ✅ |
| [moment](https://www.npmjs.com/package/moment) | ✅ |
| [nprogress](https://www.npmjs.com/package/nprogress) | ✅ | [rstacruz/nprogress#218](https://github.com/rstacruz/nprogress/pull/218) |
| [popper.js](https://www.npmjs.com/package/popper.js) | ✅ |
| [preact](https://www.npmjs.com/package/preact) | ✅ |
| [ramda](https://www.npmjs.com/package/ramda) | ✅ |
| [react](https://www.npmjs.com/package/react) | ❌ | [facebook/react#11503](https://github.com/facebook/react/issues/11503) |
| [rxjs](https://www.npmjs.com/package/rxjs) | ❌ | [ReactiveX/rxjs#4416](https://github.com/ReactiveX/rxjs/issues/4416) |
| [@stripe/stripe-js](https://www.npmjs.com/package/@stripe/stripe-js) | ✅ |
| [vue](https://www.npmjs.com/package/vue) | ✅ |
| [whatwg-fetch](https://www.npmjs.com/package/whatwg-fetch) | ✅ |

Anything that exports a standard ECMAScript module should work just fine. Please report any issues with the name and version of the problematic package.

## See Also

- [@commonshost/manifest](https://www.npmjs.com/package/@commonshost/manifest) - Auto-generate HTTP/2 Server Push Manifests
- [Commons Host](https://commons.host) - Static site hosting & CDN with [Server Push Diary](https://help.commons.host/server/configuration/server/#push) support to avoid over-push.
